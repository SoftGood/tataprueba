package co.com.tata.dto;

import co.com.tata.model.Empleado;

public class EmpleadoPorDepartamento {

	private Long empleado;
	private Long departamento;
	private Long funcion;
	private Empleado empleadoEntidad;

	public Long getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Long empleado) {
		this.empleado = empleado;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}

	public Long getFuncion() {
		return funcion;
	}

	public void setFuncion(Long funcion) {
		this.funcion = funcion;
	}

	public Empleado getEmpleadoEntidad() {
		return empleadoEntidad;
	}

	public void setEmpleadoEntidad(Empleado empleadoEntidad) {
		this.empleadoEntidad = empleadoEntidad;
	}
	
	

}
