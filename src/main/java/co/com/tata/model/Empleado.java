package co.com.tata.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Empleado")
public class Empleado implements Serializable {

	private static final long serialVersionUID = 9192885566344476679L;

	private Long id;
	private String nombre;
	private String apellidos;
	private Long numerodocumento;
	private String telefono;
	private Boolean activo;
	private Double salario;
	private Departamentos departamento;
	private String correo;

	@Id
	@Column(nullable = false, insertable = true, updatable = false)
	public Long getNumerodocumento() {
		return numerodocumento;
	}

	public void setNumerodocumento(Long numerodocumento) {
		this.numerodocumento = numerodocumento;
	}

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, insertable = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(nullable = false, insertable = true, updatable = true)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false, insertable = true, updatable = true)
	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	@Column(nullable = true, insertable = true, updatable = true)
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(nullable = false, insertable = true, updatable = true)
	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Column(nullable = false, insertable = true, updatable = true)
	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	@OneToOne
	@JoinColumn(name = "departamento", unique = true)
	public Departamentos getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamentos departamento) {
		this.departamento = departamento;
	}

	@Column(nullable = true, insertable = true, updatable = true)
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

}
