package co.com.tata.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "empleadoporfunciones")
public class EmpleadoPorFunciones implements Serializable {

	private static final long serialVersionUID = -7412139515210052394L;
	private Long id;
	private Empleado empleado;
	private Funciones funcion;
	
	
	public EmpleadoPorFunciones() {
		super();
	}

	public EmpleadoPorFunciones(Empleado empleado, Funciones funcion) {
		super();
		this.empleado = empleado;
		this.funcion = funcion;
	}

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, insertable = true, updatable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "empleado", unique = true)
	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	/** cascade = CascadeType.ALL, */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "funcion", unique = true)
	public Funciones getFuncion() {
		return funcion;
	}

	public void setFuncion(Funciones funcion) {
		this.funcion = funcion;
	}

}
