package co.com.tata.services;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.tata.accessdao.FuncionesDao;
import co.com.tata.dto.EmpleadoPorDepartamento;
import co.com.tata.model.Funciones;

@RestController
@RequestMapping("/funciones")
public class FuncionServices {
	
	@Autowired
	private FuncionesDao funcionesDao;
	
	@PostMapping(path = "/obtenerFunciones", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public List<Funciones> obtenerFunciones(@RequestBody EmpleadoPorDepartamento departamento) {
		return funcionesDao.funcionPorDepartamento(departamento.getDepartamento());
	}

}
