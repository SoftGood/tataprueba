package co.com.tata.services;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import co.com.tata.accessdao.EmpleadosDao;
import co.com.tata.dto.EmpleadoDto;
import co.com.tata.dto.EmpleadoPorDepartamento;
import co.com.tata.dto.RespuestaDto;
import co.com.tata.model.Empleado;

@RestController
@RequestMapping("/empleados")
public class EmpleadoServices {

	@Autowired
	private EmpleadosDao empleadosDao;

	private Gson gson = new Gson();

	@PostMapping(path = "/obtenerEmpleados", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Object> obtenerEmpleados() {
		return empleadosDao.obtenerEmpleados();
	}

	@PostMapping(path = "/agregarEmpleado", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Empleado> agregarEmpleado(@RequestBody EmpleadoDto empleados) {
		Empleado data = gson.fromJson(gson.toJson(empleados), Empleado.class);
		return empleadosDao.registrarEmpleado(data);
	}

	@PostMapping(path = "/actualizarEmpleado", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Empleado> actualizarEmpleado(@RequestBody Empleado informacion) {
		return empleadosDao.actualizarEmpleado(informacion);
	}

	@PostMapping(path = "/asociarFuncionEmpleado", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Object> asociarFuncionEmpleado(@RequestBody EmpleadoPorDepartamento informacion) {
		return empleadosDao.asociarFuncionEmpleado(informacion);
	}

}
