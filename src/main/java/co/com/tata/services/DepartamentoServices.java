package co.com.tata.services;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.tata.accessdao.DepartamentosDao;

@RestController
@RequestMapping("/departamento")
public class DepartamentoServices {

	@Autowired
	private DepartamentosDao departamentosDao;

	@PostMapping(path = "/promedioDepartamento", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public List<Object> promedioDepartamento() {
		return departamentosDao.promedioPorDepartamento();
	}
}
