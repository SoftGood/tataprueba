package co.com.tata.accessdao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.tata.dto.EmpleadoPorDepartamento;
import co.com.tata.dto.RespuestaDto;
import co.com.tata.model.Departamentos;
import co.com.tata.model.Empleado;
import co.com.tata.model.EmpleadoPorFunciones;
import co.com.tata.model.Funciones;
import co.com.tata.repository.DepartamentoRepository;
import co.com.tata.repository.EmpleadoFuncionesRepository;
import co.com.tata.repository.EmpleadoRepository;
import co.com.tata.repository.FuncionesRepository;

@Service
@Scope("prototype")
public class EmpleadosDao {

	@Autowired
	private EmpleadoRepository empleadoRepository;

	@Autowired
	private DepartamentoRepository departamentoRepository;

	@Autowired
	private EmpleadoFuncionesRepository empleFuncionesRepository;

	@Autowired
	private FuncionesRepository funcionesRepository;

	private static final Logger logger = LogManager.getLogger(EmpleadosDao.class);
	private String log = "";

	public RespuestaDto<Object> obtenerEmpleados() {
		RespuestaDto<Object> respuesta = new RespuestaDto<>();
		log = String.format("id[%s] Entro a consultar los empleados", Thread.currentThread().getId());
		logger.info(log);

		try {

			List<Empleado> listaEmpleados = empleadoRepository.findAll();
			List<Departamentos> listaDepartamentos = departamentoRepository.findAll();
	
			List<Object> listadoFinal = new ArrayList<>();
			List<Object> empleadoFinal = new ArrayList<>();

			if (listaEmpleados.isEmpty()) {
				respuesta.setExitoso(false);
				respuesta.setMensaje("No se encontraron empleados registrados.");

			} else {
				respuesta.setExitoso(true);
				
				for(Empleado selempleados: listaEmpleados) {
					empleadoFinal.add(Arrays.asList(selempleados,empleFuncionesRepository.buscarFuncionesEmpleado(selempleados)));
				}
				
				listadoFinal.add(empleadoFinal);
				respuesta.setMensaje("Se consultó con éxito los empleados.");
			}
			
			listadoFinal.add(listaDepartamentos);
			respuesta.setData(listadoFinal);

			log = String.format("id[%s] %s ", Thread.currentThread().getId(), respuesta.getMensaje());
			logger.info(log);

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje("Se presentó un problema consultando los empleados");
			log = String.format("id[%s] Se presento un problema consultando los empleados | %s",
					Thread.currentThread().getId(), e.getMessage());
			logger.info(log);
		}

		return respuesta;
	}

	@Transactional(rollbackFor = { Exception.class })
	public RespuestaDto<Empleado> registrarEmpleado(Empleado empleado) {

		RespuestaDto<Empleado> respuesta = new RespuestaDto<>();

		log = String.format("id[%s] Entro a registrar el empleado | %s", Thread.currentThread().getId(),
				empleado.getNumerodocumento());
		logger.info(log);

		try {

			Departamentos departamento = departamentoRepository.findOne(empleado.getDepartamento().getCodigo());

			if (!empleadoRepository.exists(empleado.getNumerodocumento())) {

				empleado.setDepartamento(departamento);

				empleadoRepository.save(empleado);

				respuesta.setExitoso(true);
				respuesta.setMensaje("Se agrego con éxito el empleado");

			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("El documento del empleado ya se encuentra registrado");
			}

			log = String.format("id[%s] %s", Thread.currentThread().getId(), respuesta.getMensaje());
			logger.info(log);

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje("Se presentó un problema agregando el empleado");
			log = String.format("id[%s] Se presento un problema agregando el empleado | %s",
					Thread.currentThread().getId(), e.getMessage());
			logger.info(log);
		}

		return respuesta;

	}

	/**@Transactional(rollbackFor = { Exception.class })
	public RespuestaDto<Empleado> asociarEmpleadoDepartamento(EmpleadoPorDepartamento peticion) {

		RespuestaDto<Empleado> respuesta = new RespuestaDto<>();

		log = String.format("id[%s] Entro para asociar el empleado al departamento | %s - %s",
				Thread.currentThread().getId(), peticion.getEmpleado(), peticion.getDepartamento());
		logger.info(log);

		try {

			Empleado empleado = empleadoRepository.findOne(peticion.getEmpleado());

			if (empleado != null) {

				empleFuncionesRepository.delete(empleFuncionesRepository.buscarFuncionesEmpleado(empleado));

				Departamentos departamento = departamentoRepository.findOne(peticion.getDepartamento());

				empleado.setDepartamento(departamento);

				empleadoRepository.saveAndFlush(empleado);

				respuesta.setExitoso(true);
				respuesta.setMensaje("Se asocio con éxito el empleado al departamento");

			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("El empleado seleccionado no existe");

			}

			log = String.format("id[%s] %s", Thread.currentThread().getId(), respuesta.getMensaje());
			logger.info(log);

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje("Se presentó un problema asociando el empleado");
			log = String.format("id[%s] Se presento un problema asociando el empleado | %s",
					Thread.currentThread().getId(), e.getMessage());
			logger.info(log);
		}

		return respuesta;
	}
	*/

	@Transactional(rollbackFor = { Exception.class })
	public RespuestaDto<Empleado> actualizarEmpleado(Empleado peticion) {

		RespuestaDto<Empleado> respuesta = new RespuestaDto<>();

		log = String.format("id[%s] Entro para actualizar el empleado | %s ", Thread.currentThread().getId(),
				peticion.getNumerodocumento());
		logger.info(log);

		try { 
			Empleado empleado = empleadoRepository.findOne(peticion.getNumerodocumento());

			if (empleado != null) {

				if (!empleado.getDepartamento().getCodigo().equals(peticion.getDepartamento().getCodigo())) {
					empleFuncionesRepository.delete(empleFuncionesRepository.buscarFuncionesEmpleado(peticion));
				}

				empleadoRepository.saveAndFlush(peticion);

				respuesta.setExitoso(true);
				respuesta.setMensaje("Se actualizó con éxito el empleado");

			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("El empleado ya no se encuentra disponible");

			}

			log = String.format("id[%s] %s", Thread.currentThread().getId(), respuesta.getMensaje());
			logger.info(log);

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje("Se presentó un problema actualizando el empleado");
			log = String.format("id[%s] Se presento un problema actualizando el empleado | %s",
					Thread.currentThread().getId(), e.getMessage());
			logger.info(log);
		}

		return respuesta;
	}

	@Transactional(rollbackFor = { Exception.class })
	public RespuestaDto<Object> asociarFuncionEmpleado(EmpleadoPorDepartamento peticion) {

		RespuestaDto<Object> respuesta = new RespuestaDto<>();

		log = String.format("id[%s] Entro para asociar la funcion al empleado | %s - %s",
				Thread.currentThread().getId(), peticion.getFuncion(), peticion.getEmpleado());
		logger.info(log);

		try {

			Empleado empleado = empleadoRepository.findOne(peticion.getEmpleado());

			Funciones funcion = funcionesRepository.findOne(peticion.getFuncion());

			if (empleado == null) {
				respuesta.setExitoso(false);
				respuesta.setMensaje("El empleado seleccionado no existe.");
			} else if (funcion == null) {
				respuesta.setExitoso(false);
				respuesta.setMensaje("La funcion seleccionada no existe.");
			} else if (funcion.getDepartamento() == empleado.getDepartamento()) {
				empleFuncionesRepository.save(new EmpleadoPorFunciones(empleado, funcion));
				respuesta.setExitoso(true);
				respuesta.setMensaje("Se asocio con éxito la funcion.");
			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("La funcion no corresponde al departamento del empleado.");
			}

			log = String.format("id[%s] %s | %s ", Thread.currentThread().getId(), respuesta.getMensaje(),
					peticion.getFuncion());
			logger.info(log);

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje("Se presentó un problema asociando la funcion al empleado");
			log = String.format("id[%s] Se presento un problema asociando la funcion al empleado | %s",
					Thread.currentThread().getId(), e.getMessage());
			logger.info(log);
		}

		return respuesta;

	}

}
