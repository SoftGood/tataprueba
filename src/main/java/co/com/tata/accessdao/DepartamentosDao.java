package co.com.tata.accessdao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.com.tata.model.Departamentos;
import co.com.tata.repository.DepartamentoRepository;

@Service
@Scope("prototype")
public class DepartamentosDao {

	@Autowired
	private DepartamentoRepository departamentoRepository;

	public List<Object> promedioPorDepartamento() {

		return departamentoRepository.promedioDepartamento();
	}
	
	public Departamentos departamentoPorId(Long id) {

		return departamentoRepository.findOne(id);	
	}
}
