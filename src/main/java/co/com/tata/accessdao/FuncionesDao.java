package co.com.tata.accessdao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.com.tata.model.Departamentos;
import co.com.tata.model.Funciones;
import co.com.tata.repository.FuncionesRepository;

@Service
@Scope("prototype")
public class FuncionesDao {
	
	@Autowired
	private FuncionesRepository funcionesRepository;
	
	@Autowired
	private DepartamentosDao departamentosDao;
	
	public List<Funciones> obtenerFunciones() {
		return funcionesRepository.findAll();
	}
	
	public List<Funciones> funcionPorDepartamento(Long departamento) {
		Departamentos info =departamentosDao.departamentoPorId(departamento);
		return funcionesRepository.buscarFuncionesDepartamento(info);
	}
}
