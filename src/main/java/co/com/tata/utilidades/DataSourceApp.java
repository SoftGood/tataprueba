package co.com.tata.utilidades;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@ConfigurationProperties(prefix = "conexionBD.datasource")
@Service
public class DataSourceApp extends HikariConfig {

	@Bean(name = "hikari-conexion")
	@Primary
	public DataSource tataDataSource() {
		return new HikariDataSource(this);
	}

}
