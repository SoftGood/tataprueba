package co.com.tata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.com.tata.model.Departamentos;

@Repository
public interface DepartamentoRepository extends JpaRepository<Departamentos, Long> {

	@Query("SELECT  e.departamento, AVG(e.salario) FROM Empleado e, Departamentos d WHERE e.departamento = d.codigo GROUP BY e.departamento")
	List<Object> promedioDepartamento();
}
