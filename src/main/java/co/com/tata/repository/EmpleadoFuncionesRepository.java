package co.com.tata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.tata.model.Empleado;
import co.com.tata.model.EmpleadoPorFunciones;

@Repository
public interface EmpleadoFuncionesRepository extends JpaRepository<EmpleadoPorFunciones, Long> {
	@Query("Select e.funcion From EmpleadoPorFunciones e Where e.empleado = :pEmpleado")
	List<EmpleadoPorFunciones> buscarFuncionesEmpleado(@Param("pEmpleado") Empleado empleado);
}
