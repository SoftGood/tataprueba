package co.com.tata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.tata.model.Empleado;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> { }
