package co.com.tata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.tata.model.Departamentos;
import co.com.tata.model.Funciones;

@Repository
public interface FuncionesRepository extends JpaRepository<Funciones, Long> {
	@Query("Select f From Funciones f Where f.departamento = :pDepartamento")
	List<Funciones> buscarFuncionesDepartamento(@Param("pDepartamento") Departamentos departamento);
}
