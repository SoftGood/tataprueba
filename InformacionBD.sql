CREATE TABLE `departamento` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `codigo` int NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Departamento_UN` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `empleado` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) CHARACTER SET utf16le COLLATE utf16le_bin NOT NULL,
  `apellidos` varchar(100) CHARACTER SET utf16le COLLATE utf16le_bin NOT NULL,
  `telefono` varchar(12) CHARACTER SET utf16le COLLATE utf16le_bin DEFAULT NULL,
  `activo` tinyint(1) NOT NULL,
  `salario` double NOT NULL,
  `departamento` int NOT NULL,
  `correo` varchar(40) CHARACTER SET utf16le COLLATE utf16le_bin NOT NULL,
  `numerodocumento` int NOT NULL,
  PRIMARY KEY (`numerodocumento`),
  UNIQUE KEY `Empleado_UN` (`id`),
  KEY `empleado_FK` (`departamento`),
  CONSTRAINT `empleado_FK` FOREIGN KEY (`departamento`) REFERENCES `departamento` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf16le;

CREATE TABLE `empleadoporfunciones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `empleado` int NOT NULL,
  `funcion` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `empleadoporfunciones_emple_FK` (`empleado`),
  KEY `empleadoporfunciones_FK` (`funcion`),
  CONSTRAINT `empleadoporfunciones_emple_FK` FOREIGN KEY (`empleado`) REFERENCES `empleado` (`numerodocumento`),
  CONSTRAINT `empleadoporfunciones_FK` FOREIGN KEY (`funcion`) REFERENCES `funciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `funciones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `departamento` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Funciones_UN` (`id`),
  KEY `Funciones_FK` (`departamento`),
  CONSTRAINT `Funciones_FK` FOREIGN KEY (`departamento`) REFERENCES `departamento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
